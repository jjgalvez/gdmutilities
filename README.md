# gdmutilities

a collection of utilities / scripts to customize gdm

## gdmsettings.py
Change gdm default user settings
### Usage:
gdmsettings show -  show the status of current settings

Usage: gdmsettings set [OPTIONS]

  Set the value of the various gdm settings we are interested in  

Options:  
  --suspend / --suspend-off       set the suspend inactivity value for both ac  
                                  and battery  
  --suspendAC / --suspendAC-off   set the suspend inactivity value for ac  
  --suspendBattery / --suspendBattery-off  
                                  set the suspend inactivity value for battery  
  --blank_screen INTEGER RANGE    set the blank screen idle time between 1 and  
                                  15 min. 0 deactivates  
  --time_format [12h|24h]  
  --help     

  ## gdmbackground     
  set the gdm background image  
  Usage:  gdmbackground <image> 

  