#!/usr/bin/env python3

import subprocess
import os
import shutil
import sys
import re
import tkinter
import click

here = os.path.dirname(os.path.realpath(os.path.abspath(__file__)))

def makeCompositImage(image, monitors, width, height, offset):
    '''
    create a temp composit image from the supplied image image for dual screen setup
    '''
    #only import PIL is needed, this will make Pillow an optional dependancy
    from PIL import Image
    baseimg = Image.open(image)
    baseimg = baseimg.resize((offset, height)) if offset else baseimg.resize((int(width/monitors), height))
    # print(baseimg.width)
    # img = Image.new('RGB', (baseimg.width*monitors, baseimg.height))
    img = Image.new('RGB', (width, height))
    img.paste(baseimg, (0,0))
    c = 1
    while monitors > 1:
        next = offset*c if offset else baseimg.width * c
        # print(next)
        img.paste(baseimg, (next, 0))
        c += 1
        monitors -= 1
    img.save('/tmp/gdm_background.jpg')
    return '/tmp/gdm_background.jpg'


def screensize():
    '''
    return screen size
    '''
    root = tkinter.Tk()
    root.withdraw()
    w, h = root.winfo_screenwidth(), root.winfo_screenheight()
    
    del root

    return w,h

    # the rest of this stuff seems unnecessary

    # standardRes = ('640x360', '800x600', '1024x768', '1280x720',
    # '1280x800', '1280x1024', '1360x768', '1366x768', '1440x900',
    # '1536x864', '1600x900', '1680x1050', '1920x1080', '1920x1200',
    # '2048x1152', '2560x1080', '2560x1440', '3440x1440', '3840x2160', )
    
    # # check to see if your resolution is a standard one, and use it, otherwise select
    # # from a list of standard resolutions
    # if f'{w}x{h}' in standardRes:
    #     return w, h
    # else:
    #     print('Please select a standard screen resolution:')
    #     for r in standardRes:
    #         print(f'[{standardRes.index(r)}]\t{r}')
    #     a = int(input('Selection: '))
    #     if a in range(len(standardRes)):
    #         w, h = [int(x) for x in standardRes[a].split('x')]
    #         return w, h
    #     else:
    #         sys.exit('Invalid selection')

def buildgResource(image, width, height):
    '''
    build a new gresource by creating the gresouce xml file
    and editing the css file to point to a new background image
    '''

    # extract gdm theme to tmp folder
    subprocess.run(os.path.join(here, 'extractgst.sh'), shell=True)

    # copy image to created theme folder
    imageext = os.path.splitext(image)[1]
    newimagename = f'background{imageext}'
    imageabspath = os.path.abspath(image)
    shutil.copy(imageabspath,
        os.path.join('/tmp/shell-theme/theme', newimagename))
    gresources = list(os.walk('/tmp/shell-theme/theme'))

    # create resource xml file
    with open('/tmp/shell-theme/gnome-shell-theme.gresource.xml', 'w') as gxml:
        gxml.write('''<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gnome/shell/theme">\n''')
        for res in gresources:
            f2 = [os.path.join(res[0].lstrip('/tmp/shell-theme/theme'), x) for x in res[2]]
            for f in f2:
                gxml.write(f'\t<file>{f}</file>\n')
        gxml.write('''  </gresource>
</gresources>''')
    shutil.move('/tmp/shell-theme/gnome-shell-theme.gresource.xml',
        '/tmp/shell-theme/theme/gnome-shell-theme.gresource.xml')

    # edit gnome-shell css file to reference the new image file
    gnome_shell_css = open('/tmp/shell-theme/theme/gnome-shell.css').read()

    newLockDialogGroup = f'''#lockDialogGroup {{
  background: #2e3436 url(background{imageext});
  background-size: {width}px {height}px;
  background-repeat: no-repeat; }}'''

    cssnew = re.sub(r'#lockDialogGroup.*?}', newLockDialogGroup, gnome_shell_css, flags=re.S)
    with open('/tmp/shell-theme/theme/gnome-shell.css', 'w') as css:
        css.write(cssnew)

    # compile and move the gresource file
    os.chdir('/tmp/shell-theme/theme')
    subprocess.run('sudo glib-compile-resources gnome-shell-theme.gresource.xml',
        shell=True)

    # shutil.copy('gnome-shell-theme.gresource', '/usr/share/gnome-shell')
    subprocess.run('sudo mv ./gnome-shell-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource',
        shell=True)


@click.command()
@click.option('-i', '--image', required=True, help='image to set as lockscreen background')
@click.option('-m', '--monitors', default=0, help='Number of monitors in a multimonitor setup')
@click.option('-o', '--offset', default=0, help='offset is the width of the main monitor, otherswise the image are arranged automatically')
def main(image, monitors, offset):
    width, height = screensize()
    if os.path.exists(image):
        img = makeCompositImage(image, monitors, width, height, offset) if monitors else image
        buildgResource(img, width, height)
        print('restart gdm or reboot computer to take affect')
    else:
        print('usage: gdmbackground --image=<image>')

if __name__ == '__main__':
    main()
