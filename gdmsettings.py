#!/usr/bin/env python3

# This script is intended to help you change the default GDM 
# settings such as power and screen lock ect.

# Jose Galez jose@cybergalvez.com

import click
import subprocess

settings = {
    'suspend': {'org.gnome.settings-daemon.plugins.power': ['sleep-inactive-ac-type', 'sleep-inactive-battery-type']},
    'blank_screen': 'org.gnome.desktop.session idle-delay',
    # 'lock_screen': 'org.gnome.desktop.screensaver lock-enabled',
    'time_format': 'org.gnome.desktop.interface clock-format',
}


def setproperty(property, key, value):
    subprocess.call(f'sudo -u gdm dbus-launch gsettings set {property} {key} {value}', shell=True)

def checkoption(option):
    if option != None and option:
        return 'on'
    elif option != None and not option:
        return 'off'
    else:
        return False

@click.group()
def cli():
    pass


@cli.command()
def show():
    '''
    Show the current values for the gdm settings we are interested in
    '''
    for key,val in settings.items():
        # print(type(val))
        if type(val) == dict:
            for key2, val2 in val.items():
                for v in val2:
                    # print(v)
                    currentVal = subprocess.run(f'sudo -u gdm dbus-launch gsettings get {key2} {v}', shell=True, stdout=subprocess.PIPE, encoding='utf-8')
                    if currentVal.returncode == 0:
                        print(f'{key} {v}: {currentVal.stdout.strip()}')
        else:
            currentVal = subprocess.run(f'sudo -u gdm dbus-launch gsettings get {val}', shell=True, stdout=subprocess.PIPE, encoding='utf-8')
            if currentVal.returncode == 0:
                print(f'{key}: {currentVal.stdout.strip()}')


@cli.command()
@click.option('--suspend/--suspend-off', help='set the suspend inactivity value for both ac and battery', default=None)
@click.option('--suspendAC/--suspendAC-off', default=None, help='set the suspend inactivity value for ac')
@click.option('--suspendBattery/--suspendBattery-off', default=None, help='set the suspend inactivity value for battery')
@click.option('--blank_screen', type=click.IntRange(0,15), help='set the blank screen idle time between 1 and 15 min. 0 deactivates')
@click.option('--time_format', type=click.Choice(['12h', '24h']))
def set(suspend, suspendac, suspendbattery, blank_screen, time_format):
    '''
    Set the value of the various gdm settings we are interested in
    '''
    # print((suspend, suspendac, suspendbattery, blank_screen, time_format))

    # set / unset suspend
    if checkoption(suspend) == 'on':
        print('Set Suspend on')
        property = [*settings['suspend']][0]
        for key in settings['suspend'][property]:
            setproperty(property, key, 'suspend')

    if checkoption(suspend) == 'off':
        print('Set Suspend off')
        property = [*settings['suspend']][0]
        for key in settings['suspend'][property]:
            setproperty(property, key, 'nothing')

    # set / unset suspendAC
    if checkoption(suspendac) == 'on':
        print('Set Suspend AC on')
        property = [*settings['suspend']][0]
        setproperty(property, settings['suspend'][property][0], 'suspend')

    if checkoption(suspendac) == 'off':
        print('Set Suspend AC off')
        property = [*settings['suspend']][0]
        setproperty(property, settings['suspend'][property][0], 'nothing')

    # set / unset suspendBattery
    if checkoption(suspendbattery) == 'on':
        print('Set Suspend AC on')
        property = [*settings['suspend']][0]
        setproperty(property, settings['suspend'][property][1], 'suspend')

    if checkoption(suspendbattery) == 'off':
        print('Set Suspend AC off')
        property = [*settings['suspend']][0]
        setproperty(property, settings['suspend'][property][1], 'nothing')

    # set / unset bland_screen
    if blank_screen != None:
        print(f'Setting screen idle to: {blank_screen}')
        idletime = blank_screen * 60
        property, key = settings['blank_screen'].split()
        setproperty(property, key, idletime)

    # set time_format
    if time_format != None:
        print(f'Setting time format to: {time_format}')
        property, key = settings['time_format'].split()
        setproperty(property, key, time_format)

        
if __name__ == '__main__':
    cli()